﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace projectlab
{
    public partial class ImageProc : Form
    {
        Form Form;

        Image image;

        Image<Bgr, Byte> ori;
        Image<Gray, Byte> smooth;
        Image<Gray, float> gray;
        Image<Gray, float> thres;

        enum MainImage { ori, smooth, gray, thres };

        MainImage flag;


        public ImageProc()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Form.Show();
        }

        private void ImageProc_Load(object sender, EventArgs e)
        {
            resetControl();
            // new eventhandler for multiple changed values
            var valuesChanged = new System.EventHandler(this.valuesChanged);
            this.trackTop.ValueChanged += valuesChanged;
            this.trackBottom.ValueChanged += valuesChanged;
        }

        private void valuesChanged(object sender, EventArgs e)
        {
            if (this.image != null)
            {
                debug.Text = Convert.ToString(e);
            }
            else
            {
                this.image = Properties.Resources.cat;

            }
        }


        private void resetControl()
        {
            gbEditor.Enabled = true;

            lblTop.Hide();
            trackTop.Hide();

            lblBottom.Hide();
            trackBottom.Hide();

            this.lblSmooth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular);
            this.lblGray.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular);
            this.lblThres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular);

            this.lblSmooth.ForeColor = System.Drawing.Color.Black;
            this.lblGray.ForeColor = System.Drawing.Color.Black;
            this.lblThres.ForeColor = System.Drawing.Color.Black;

            this.flag = MainImage.ori;

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "jpg|*.jpg" +
                                   "| jpeg|*.jpeg" +
                                   "| png|*.png" +
                                   "| bmp|*.bmp";

            var result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.image = Image.FromFile(openFileDialog1.FileName);
                firstLoadImage();
            }
        }
        private void firstLoadImage()
        {
            Image<Gray, byte> gray;
            Image<Gray, float> grayFloat;

            this.ori = new Image<Bgr, byte>(new Bitmap(this.image));
            gray = new Image<Gray, byte>(this.ori.Width, this.ori.Height);
            this.canny = gray.Clone();

            CvInvoke.cvCvtColor(this.ori, gray, COLOR_CONVERSION.CV_BGR2GRAY);
            grayFloat = gray.Convert<Gray, float>();

            CvInvoke.cvCanny(gray, this.canny, 150, 60, 3);
            this.laplace = grayFloat.Laplace(7);
            this.sobel = grayFloat.Sobel(1, 0, 3);

            imgMain.Image = this.ori.ToBitmap();
            imgCanny.Image = this.canny.ToBitmap();
            imgLaplace.Image = this.laplace.ToBitmap();
            imgSobel.Image = this.sobel.ToBitmap();
        }
    }
}
